#include<stdio.h>
void swap(int *a,int *b)
{
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;
}

int main()
{
   int x,y;
   printf("enter 2 numbers \n");
   scanf("%d%d",&x,&y);
   swap(&x,&y);
   printf("after swap x=%d \n y=%d",x,y);
   return 0;
}
